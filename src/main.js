import Vue from 'vue'
import App from './App.vue'
// Bootstrap
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import VueRouter from 'vue-router'
import questions from './components/questions'
import helloWorld from './components/HelloWorld'
import results from './components/Results'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(VueRouter)
Vue.use(BootstrapVue);
Vue.use(IconsPlugin)





const routes = [
  {
    path: "/hello",
    name: "helloWorld",
    component: helloWorld
  },
  {
    path: "/questions",
    name: 'questions',
    component: questions
  },
  {
    path: "/results",
    name: "results",
    component: results
  },
  {
    path: '/',
    redirect: "/hello"
  }
]

Vue.config.productionTip = false

const router = new VueRouter({routes})

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
