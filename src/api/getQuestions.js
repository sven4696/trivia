export const apiQuestions = () => {

    return fetch('https://opentdb.com/api.php?amount=10&category=18&difficulty=medium&type=boolean')
    .then(response => response.json())
    .then(data => {
        console.log(data)
        return data.results
    })
}